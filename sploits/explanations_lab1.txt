#Yu Li, 1000173554, e-mail durant.li@mail.utornto.ca
#Min Wu, 1000509684, e-mail len.wu@mail.utoronto.ca

sploit1: The source code has a strcpy function without boundary check, which has a potential buffer overflow vulnerability. Then, we used gdb to set breakpoint at target1. We found that the return address is 0x2021fe88 and the buffer is located at 0x2021fe10. The distance between return address and buffer is 120 bytes. We put 23 bytes NOPs at the beginning of buffer followed by shellcode which has 45 bytes. Then, we filled the buffer address to the rest of buffer so that it can overwrite return address. We made return address aligned to make sure no illegal instructions.

sploit2: The source code has boundary check. Therefore, we need to overwrite length first so that we can reach the return address. The return address is 0x2021fe58, buf is located at 0x2021fd40, i is 0x2021fe48 and len is 0x2021fe4c. We overwrite i to 267 to jump to address of len. We overwrite len to 284 to reach return address. We put return address in environment variable.

sploit3: The buffer is located in foo function, we need to overwrite the foo's return address. Return address is 0x2021fe58 and buffer is located at 0x2021fe10. The return address is 72 bytes away from buffer. We put few NOPs at the beginning of our attack string, followed by the shellcode. Then, we filled buffer address to the rest of attack string so that it can overflow return address.

sploit4: This is similar to sploit2. The target did boundary check, but we can still overflow the len. We can modify i and len to reach return address. The return address is 0x2021fe68, i is 0x2021fe5c, len is 0x2021fe58 and buf is 0x2021fdb0. We put buffer address in environment variable so that it can overflow return address.
